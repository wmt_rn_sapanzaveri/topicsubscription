import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();


Future<void> firebaseHandler(RemoteMessage message) async {
  print("Message from Push Notifcation${message.notification!.title}");
  var androidDetails = const AndroidNotificationDetails(
    "First_Notic",
    "General Notifications",
    channelDescription: "This is FTS",
    importance: Importance.max,
    playSound: true,
    priority: Priority.high,
  );
  var iSODetails = new IOSNotificationDetails();
  var generalNotificationDetails =
  NotificationDetails(android: androidDetails, iOS: iSODetails);
  // await flutterLocalNotificationsPlugin.show(
  //     0, "${message.data}", "${message.data}",
  //     generalNotificationDetails);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  var initAndroidSetting = AndroidInitializationSettings('wsign');
  var initiOSSetting = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification:
          (int? id, String? title, String? body, String? payload) async {});
  var initializationSettings =
  InitializationSettings(android: initAndroidSetting, iOS: initiOSSetting);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String? payload) async {
        if (payload != null) {
          print("Notification Payload $payload");
        }
      });
  FirebaseMessaging.onBackgroundMessage(firebaseHandler);

  // FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
  //   print("Message from Push Notifcation from Listen ${message.data}");
  //   var androidDetails = const AndroidNotificationDetails(
  //     "First_Notic",
  //     "General Notifications",
  //     channelDescription: "This is FTS",
  //     importance: Importance.max,
  //     priority: Priority.high,
  //   );
  //   var iSODetails = new IOSNotificationDetails();
  //   var generalNotificationDetails =
  //   NotificationDetails(android: androidDetails, iOS: iSODetails);
  //   await flutterLocalNotificationsPlugin.show(
  //       0, "${message.data["title"]}", "${message.data["body"]}",
  //       generalNotificationDetails);
  // });

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;


  Future<void> _incrementCounter() async {
    await FirebaseMessaging.instance.subscribeToTopic("getNotification");
    setState(() {
      _counter++;
    });
  }

  @override
  void initState() {
fetchNotification();
    // FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
    //   print("Message from Push Notifcation from Listen ${message.notification!.body}");
    //   var androidDetails = const AndroidNotificationDetails(
    //     "First_Notic",
    //     "General Notifications",
    //     channelDescription: "This is FTS",
    //     importance: Importance.max,
    //     priority: Priority.high,
    //   );
    //   var iSODetails = new IOSNotificationDetails();
    //   var generalNotificationDetails =
    //   NotificationDetails(android: androidDetails, iOS: iSODetails);
    //   await flutterLocalNotificationsPlugin.show(
    //       0, "${message.notification!.title}", "${message.notification!.body}",
    //       generalNotificationDetails);
    // });
    super.initState();
  }
  Future<File> getImagePath(String url) async {
    final http.Response response = await http.get(Uri.parse(url));
    Uint8List resizedData = (response.bodyBytes);

    var byteData = await FlutterImageCompress.compressWithList(
      resizedData,
    );
    final file = File((await getTemporaryDirectory()).path);
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return file;
  }

  Future<Uint8List> _getByteArrayFromUrl(String url) async {
    final http.Response response = await http.get(Uri.parse(url));
    Uint8List resizedData = (response.bodyBytes);

    var result = await FlutterImageCompress.compressWithList(
      resizedData,
    );
    // final file = File((await getTemporaryDirectory()).path);
    // await file.writeAsBytes(result.buffer.asUint8List(result.offsetInBytes, result.lengthInBytes));

    // print(result.length)
    return result;
    // return response.bodyBytes;
  }
  fetchNotification() {
    // if(Platform.isIOS){
    FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    // }
    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      final ByteArrayAndroidBitmap largeIcon = ByteArrayAndroidBitmap(
          await _getByteArrayFromUrl(
              "${message.notification!.android!.imageUrl}"));
      final ByteArrayAndroidBitmap bigPicture = ByteArrayAndroidBitmap(
          await _getByteArrayFromUrl(
              '${message.notification!.android!.imageUrl}'));
      final BigPictureStyleInformation bigPictureStyleInformation =
      BigPictureStyleInformation(bigPicture,
          contentTitle: '${message.notification!.title}',
          htmlFormatContentTitle: true,
          summaryText: '${message.notification!.body}',
          htmlFormatSummaryText: true);
      print(
          "Message from Push Notifcation from Listen Method${message.notification!.android!.imageUrl}");
      // Android
      var androidDetails = AndroidNotificationDetails(
        "Catalog Alert",
        "Offer Notifications",
        channelDescription: "Offer Notification from the Store",
        importance: Importance.max,
        priority: Priority.high,
        channelShowBadge: true,
        fullScreenIntent: true,
        playSound: true,
        enableVibration: true,
        enableLights: true,
        autoCancel: true,
        largeIcon: largeIcon,
        styleInformation: bigPictureStyleInformation,
      );
      File file =
      await getImagePath("${message.notification!.apple!.imageUrl}");

      var iSODetails = IOSNotificationDetails(
          attachments: [IOSNotificationAttachment("${file.path}")],
          presentAlert: true,
          presentBadge: true,
          presentSound: true);
      var generalNotificationDetails =
      NotificationDetails(android: androidDetails, iOS: iSODetails);

      await flutterLocalNotificationsPlugin.show(
          0,
          "${message.notification!.title}",
          "${message.notification!.body}",
          generalNotificationDetails);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme
                  .of(context)
                  .textTheme
                  .headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
